#! /usr/bin/env bash

export YCAST_STATIONS=${PWD}/stations

function ycast_podman()
{
    podman run --rm -ti -v${YCAST_STATIONS}:/opt/ycast/stations -p8010:8010 registry.gitlab.com/tjuerges/ycast:2022-08-06-11.38.16
}

function ycast_venv()
{
    [[ ! -d ycast-env ]] && python3 -m venv ycast-env
    . ycast-env/bin/activate
    [[ ! -d ycast-env/lib/python3.9/site-packages/ycast ]] && python3 -m pip install ycast
    python3 -m ycast -p 8010 -c ${YCAST_STATIONS}
    deactivate
}

ycast_podman
#ycast_venv
